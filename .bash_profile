export PATH=/usr/local/bin:/bin:/sbin:/usr/bin:/usr/local/sbin:$PATH
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_112.jdk/Contents/Home
export EDITOR='subl -w'
export PATH="$(yarn global bin):$PATH"

# Git branch in prompt.
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\[\033[31m\]A.C.: \[\033[32m\]\W\[\033[34m\]\$(parse_git_branch)\[\033[33m\] >\[\033[m\] "

# Colors for mac
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# new colorful ls
alias ls='ls -GFh'

export PATH=$PATH:~/Work/www/playActivator

# This is an alias to kill the dock if it blocks
alias resetDock="rm ~/Library/Application\ Support/Dock/*.db ; killall Dock"

# Other aliases
alias gw='./gradlew'
alias gdelete='gstal $1 | xargs -n 1 git branch -d'
alias bhredock='dev stop blackhole-sync && yes | dev rm -v blackhole-sync && gw dockerTag --parallel && dev up'
alias redock='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

# My git aliases
alias gc='git commit'
alias gs='git status'
alias gdf='git diff'
alias ga='git add'
alias gtree='git log --graph --decorate --oneline'
alias hgrep='history | grep'
alias ll='ls -l'

# Global npm brew thing
# export PATH="$HOME/.npm-packages/bin:$PATH"

# History controls
export HISTFILESIZE=1000000
export HISTSIZE=1000000
export SHELL_SESSION_HISTORY=0
# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups  
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend
# After each command, append to the history file and reread it
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

# Cleaning old branches
gstale() {
    if [ "$1" = "" ]; then
        git branch --merged | grep -v "\*"
    else
        git branch --merged $1 | grep -v "\*$1"
    fi
}

# yarn in global path
export PATH="$(yarn global bin):$PATH"
source /usr/local/dev-env/bin/profile

# add subl . command
export PATH=/bin:/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:$PATH
export EDITOR='subl -w'
